import pytest
import qcast.sample_testing_files.hello_world as hw

def test_hello_world():
    result = hw.hello_world()
    assert result == "Hello World"